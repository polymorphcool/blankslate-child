</div>
<footer id="footer">
<?php if ( is_active_sidebar( 'primary-widget-area' ) ) : ?>
	<div id="primary">
		<ul class="xoxo">
		<?php dynamic_sidebar( 'primary-widget-area' ); ?>
		</ul>
	</div>
<?php endif; ?>
<div id="copyright">
&copy; <?php echo esc_html( date_i18n( __( 'Y', 'blankslate' ) ) ); ?> <?php echo esc_html( get_bloginfo( 'name' ) ); ?>
</div>
</footer>
</div>
<?php wp_footer(); ?>
</body>
</html>